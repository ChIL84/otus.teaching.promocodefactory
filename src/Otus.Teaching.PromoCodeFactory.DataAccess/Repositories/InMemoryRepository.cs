﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    /// <summary>
    /// Универсальный репозиторий объектов, хранимых и обрабатываемых в оперативной памяти
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <remarks>Репозиторий только для тестов</remarks>
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        #region состояние

        /// <summary>
        /// Коллекция объектов
        /// </summary>
        protected IEnumerable<T> Data { get; set; }

        #endregion

        #region конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="data"></param>
        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        #endregion

        #region методы репозитория (реализация IRepository)

        /// <summary>
        /// Получить все объекты репозитория
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        /// <summary>
        /// Получить объект репозитория по Id
        /// </summary>
        /// <param name="guid">GUID объекта, для которого получаем информацию</param>
        /// <returns></returns>
        public Task<T> GetByGuidAsync(Guid guid)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Guid == guid));
        }

        /// <summary>
        /// Удалить объект репозитория по Id
        /// </summary>
        /// <param name="guid">GUID удаляемого объекта</param>
        /// <returns></returns>
        public Task<bool> DeleteByGuidAsync(Guid guid)
        {
            return Task.Run<bool>(() =>
            {
                IEnumerable<T> result = Data.Where(item => item.Guid != guid);

                int sourceCountItems = Data.Count();

                int resultCountItems = result.Count();

                if (sourceCountItems <= resultCountItems)
                {
                    return false;
                }

                Data = result;

                return true;
            });
        }

        /// <summary>
        /// Добавить объект в репозиторий
        /// </summary>
        /// <param name="item">объект, добавляемый в репозиторий</param>
        /// <returns></returns>
        public Task<T> CreateAsync(T item)
        {
            return Task.Run<T>(() =>
            {
                item.Guid = Guid.NewGuid();

                Data = Data.Append(item);

                return item;
            });
        }

        /// <summary>
        /// Обновить объект в репозитории
        /// </summary>
        /// <param name="item">объект, обновляемый в репозитории</param>
        /// <returns></returns>
        /// <exception cref="RepositoryException"></exception>
        public Task<T> UpdateAsync(T item)
        {
            return Task.Run<T>(async () =>
            {
                T sourceItem = await GetByGuidAsync(item.Guid);

                if (sourceItem == null)
                {
                    throw new RepositoryException($"В справончике сотрудников не найдена запись " +
                        $"с идентификатором '{item.Guid}'");
                }

                sourceItem.FillBy(item);

                return sourceItem;
            });
        } 
        #endregion
    }
}