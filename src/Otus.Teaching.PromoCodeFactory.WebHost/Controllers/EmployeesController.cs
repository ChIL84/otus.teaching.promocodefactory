﻿using FluentValidation;
using FluentValidation.AspNetCore;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DTOModels;
using Otus.Teaching.PromoCodeFactory.WebHost.Transforms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    /// <remarks>контроллер сотрудников</remarks>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        #region состояние классов

        /// <summary>
        /// Репозиторий сотрудников
        /// </summary>
        private readonly IRepository<Employee> _employeeRepository;

        /// <summary>
        /// Репозиторий ролей
        /// </summary>
        private readonly IRepository<Role> _rolesRepository;

        /// <summary>
        /// Валидатор DTOEmployeeCreateRequest
        /// </summary>
        private readonly IValidator<DTOEmployeeCreateRequest> _dtoEmployeeCreateRequestValidator;

        /// <summary>
        /// Валидатор DTOEmployeeUpdateRequest
        /// </summary>
        private readonly IValidator<DTOEmployeeUpdateRequest> _dtoEmployeeUpdateRequestValidator;

        /// <summary>
        /// Журнал событий контроллера
        /// </summary>
        private readonly ILogger _logger;

        #endregion

        #region конструкор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="employeeRepository">репозиторий сотрудников</param>
        public EmployeesController(ILogger<EmployeesController> logger,
            IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository,
            IValidator<DTOEmployeeCreateRequest> dtoEmployeeCreateRequestValidator,
            IValidator<DTOEmployeeUpdateRequest> dtoEmployeeUpdateRequestValidator)
        {
            _logger = logger;
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
            _dtoEmployeeCreateRequestValidator = dtoEmployeeCreateRequestValidator;
            _dtoEmployeeUpdateRequestValidator = dtoEmployeeUpdateRequestValidator;
        }

        #endregion

        #region методы контроллера

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<DTOEmployeeResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var dtoEmployeesResponsesList = employees.Select(x =>
                x.ToDTOEmployeeResponse()).ToList();

            return dtoEmployeesResponsesList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<DTOEmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByGuidAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return employee.ToDTOEmployeeResponse();
        }

        /// <summary>
        /// Удалить данные о сотруднике по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<bool>> DeleteEmployeeByIdAsync(Guid id)
        {
            var deleteResult = await _employeeRepository.DeleteByGuidAsync(id);

            return deleteResult;
        }

        /// <summary>
        /// Создать новую запись о сотруднике
        /// </summary>
        /// <param name="dtoEmployeeCreateRequest">входные данные о новой записи о сотруднике</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<DTOEmployeeResponse>> CreateEmployeeAsync(
            DTOEmployeeCreateRequest dtoEmployeeCreateRequest)
        {
            ValidationResult result = await _dtoEmployeeCreateRequestValidator.ValidateAsync(dtoEmployeeCreateRequest);

            if (!result.IsValid)
            {
                result.AddToModelState(this.ModelState);

                return ValidationProblem();
            }

            Employee employee = null;

            try
            {
                employee = await dtoEmployeeCreateRequest.ToEmployeeAsync(_rolesRepository);
            }
            #region catches
            catch (TransformException ex)
            {
                string message = $"В методе {nameof(CreateEmployeeAsync)} " +
                    $"на шаге {nameof(TranformDTOEmployeeCreateRequestToEmployee)} возникла ошибка.";

                _logger.LogError(ex, message);

                return UnprocessableEntity();
            }
            #endregion

            try
            {
                // на этом шаге помимо внесения записи в репозиторий
                // получаем id новой записи в справочнике
                employee = await _employeeRepository.CreateAsync(employee);
            }
            #region catches
            catch (RepositoryException ex)
            {
                string message = $"В методе {nameof(CreateEmployeeAsync)} " +
                    $"на шаге {nameof(_employeeRepository.CreateAsync)} возникла ошибка.";

                _logger.LogError(ex, message);

                return UnprocessableEntity();
            }
            #endregion

            return employee?.ToDTOEmployeeResponse();
        }

        /// <summary>
        /// Обновить существующую запись о сотруднике
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<DTOEmployeeResponse>> UpdateEmployeeAsync(
            DTOEmployeeUpdateRequest dtoEmployeeUpdateRequest)
        {
            ValidationResult result = await _dtoEmployeeUpdateRequestValidator.ValidateAsync(dtoEmployeeUpdateRequest);

            if (!result.IsValid)
            {
                result.AddToModelState(this.ModelState);

                return ValidationProblem();
            }

            Employee employee = null;

            try
            {
                employee = await dtoEmployeeUpdateRequest.ToEmployeeAsync(_rolesRepository);
            }
            #region catches
            catch (TransformException ex)
            {
                string message = $"В методе {nameof(UpdateEmployeeAsync)} " +
                    $"на шаге {nameof(TranformDTOEmployeeUpdateRequestToEmployee)} возникла ошибка.";

                _logger.LogError(ex, message);

                return UnprocessableEntity();
            }
            #endregion            

            try
            {
                employee = await _employeeRepository.UpdateAsync(employee);
            }
            #region catches
            catch (RepositoryException ex)
            {
                string message = $"В методе {nameof(UpdateEmployeeAsync)} " +
                    $"на шаге {nameof(_employeeRepository.UpdateAsync)} возникла ошибка.";

                _logger.LogError(ex, message);

                return UnprocessableEntity();
            }
            #endregion

            return employee?.ToDTOEmployeeResponse();
        }

        #endregion
    }
}