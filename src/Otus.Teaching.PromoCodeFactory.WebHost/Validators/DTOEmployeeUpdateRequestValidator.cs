﻿using FluentValidation;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DTOModels;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Validators
{
    /// <summary>
    /// Валидатор EmployeeUpdateRequestValidator
    /// </summary>
    /// <remarks>для валидации используется FluentValidation</remarks>
    public class DTOEmployeeUpdateRequestValidator
        : AbstractValidator<DTOEmployeeUpdateRequest>
    {
        public DTOEmployeeUpdateRequestValidator(IRepository<Role> rolesRepository, IRepository<Employee> employeeRepository)
        {
            RuleLevelCascadeMode = CascadeMode.Stop;

            Include(new DTOEmployeeCreateRequestValidator(rolesRepository));
            
            RuleFor(x => x.Guid).NotNull().NotEmpty();

            if (employeeRepository != null)
            {
                RuleFor(x => x.Guid)
                    .MustAsync(async (Id, cancellation) =>
                    {
                        Employee employee = await employeeRepository.GetByGuidAsync(Id);

                        return employee != null;
                    })
                    .WithMessage("В справочнике не найден сотрудник с ID '{PropertyValue}'");
            }
        }
    }
}
