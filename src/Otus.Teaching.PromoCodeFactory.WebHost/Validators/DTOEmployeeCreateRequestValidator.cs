﻿using FluentValidation;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DTOModels;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Validators
{
    /// <summary>
    /// Валидатор DTOEmployeeCreateRequest
    /// </summary>
    /// <remarks>для валидации используется FluentValidation</remarks>
    public class DTOEmployeeCreateRequestValidator
        : AbstractValidator<DTOEmployeeCreateRequest>
    {
        public DTOEmployeeCreateRequestValidator(IRepository<Role> rolesRepository) 
        {
            RuleLevelCascadeMode = CascadeMode.Stop;
            
            RuleFor(x => x.FirstName).NotNull().NotEmpty();

            RuleFor(x => x.LastName).NotNull().NotEmpty();

            RuleFor(x => x.Email).NotNull().NotEmpty();

            RuleFor(x => x.Email).EmailAddress();

            RuleFor(x => x.Roles).NotNull();
            
            if (rolesRepository != null)
            {
                RuleForEach(x => x.Roles)
                    .MustAsync(async (roleId, cancellation) =>
                    {
                        Role role = await rolesRepository.GetByGuidAsync(roleId);

                        return role != null;
                    })
                    .WithMessage("В справочнике не найдена роль с ID '{PropertyValue}'");
            }

            RuleFor(x => x.AppliedPromocodesCount).NotNull().GreaterThanOrEqualTo(0);
        }
    }
}
