﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DTOModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Transforms
{
    /// <summary>
    /// Класс, отвечающий за преобразование сведений о сотрудниках, получаемых в запросах от пользователей, в другие объекты
    /// </summary>
    /// <remarks>В данном классе содержатся методы расширений для объектов класса EmployeeRequest</remarks>
    public static class TranformDTOEmployeeCreateRequestToEmployee
    {
        /// <summary>
        /// Асинхронный метод преобразования DTO объекта сведений о сотруднике из запроса в объект доменной модели
        /// </summary>
        /// <param name="dtoEmployeeRequest">сведения о сотруднике из запроса</param>
        /// <param name="rolesRepository">репозиторий справочника ролей</param>        
        /// <returns></returns>
		public async static Task<Employee> ToEmployeeAsync(this DTOEmployeeCreateRequest dtoEmployeeRequest, 
            IRepository<Role> rolesRepository)
		{
            if(rolesRepository == null)
            {
                throw new TransformException("Не настроена ссылка на справочник ролей сотрудников.");               
            }

            List<Role> rolesList = new List<Role>();

            #region заполнение rolesList

            if (dtoEmployeeRequest.Roles != null)
            {
                foreach (Guid roleId in dtoEmployeeRequest.Roles)
                {
                    Role role = await rolesRepository.GetByGuidAsync(roleId);

                    if (role == null)
                    {
                        throw new TransformException($"В справочнике не найдена роль с ID '{roleId}'");
                    }
                   
                    rolesList.Add(role);
                }
            } 

            #endregion

            return  new Employee
                {
                    FirstName = dtoEmployeeRequest.FirstName,
                    LastName = dtoEmployeeRequest.LastName,
                    Email = dtoEmployeeRequest.Email,
                    AppliedPromocodesCount = dtoEmployeeRequest.AppliedPromocodesCount,
                    Roles = rolesList
                };
        }
	}
}
