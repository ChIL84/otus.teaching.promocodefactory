﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DTOModels;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Transforms
{
    /// <summary>
    /// Класс, отвечающий за преобразование сведений о сотрудниках, получаемых в запросах от пользователей, в другие объекты
    /// </summary>
    /// <remarks>В данном классе содержатся методы расширений для объектов класса EmployeeRequest</remarks>
    public static class TranformDTOEmployeeUpdateRequestToEmployee
    {
        /// <summary>
        /// Асинхронный метод преобразования объекта сведений о сотруднике из запроса в объект доменной модели
        /// </summary>
        /// <param name="dtoEmployeeRequest">сведения о сотруднике из запроса</param>
        /// <param name="rolesRepository">репозиторий справочника ролей</param>        
        /// <returns></returns>
		public async static Task<Employee> ToEmployeeAsync(this DTOEmployeeUpdateRequest dtoEmployeeRequest, 
            IRepository<Role> rolesRepository)
		{
            Employee employee = await ((DTOEmployeeCreateRequest) dtoEmployeeRequest).ToEmployeeAsync(rolesRepository);

            if (employee != null)
            {
                employee.Guid = dtoEmployeeRequest.Guid;
            }

            return employee;
        }
	}
}
