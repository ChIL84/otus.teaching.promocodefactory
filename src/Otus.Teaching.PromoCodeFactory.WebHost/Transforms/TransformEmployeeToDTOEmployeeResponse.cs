﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DTOModels;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Transforms
{
    /// <summary>
    /// Класс преобразователей объектов Employee в DTOEmployeeResponse
    /// </summary>
    public static class TransformEmployeeToDTOEmployeeResponse
    {
        /// <summary>
        /// Преобразовать Employee в DTOEmployeeResponse
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public static DTOEmployeeResponse ToDTOEmployeeResponse(this Employee employee) 
        { 
            return new DTOEmployeeResponse()
            {
                Guid = employee.Guid,
                Email = employee.Email,
                Roles = employee.Roles
                    ?.Where(x=> x != null)
                    ?.Select(x => new DTORoleResponse()
                    {
                        Guid = x.Guid,
                        Name = x.Name,
                        Description = x.Description
                    })
                    ?.ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }
    }
}
