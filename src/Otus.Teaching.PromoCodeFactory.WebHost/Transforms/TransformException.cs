﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Transforms
{
    /// <summary>
    /// Класс исключения слоя преобразования данных
    /// </summary>
    public class TransformException: Exception
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">сообщение об исключении</param>
        /// <param name="innerException">внутренее исключение</param>
        public TransformException(string message = null, Exception innerException = null)
            :base(message, innerException) { }
    }
}
