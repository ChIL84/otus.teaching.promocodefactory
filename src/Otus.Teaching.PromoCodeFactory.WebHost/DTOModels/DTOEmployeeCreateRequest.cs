﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DTOModels
{
    /// <summary>
    /// DTO класс сведений о новом сотрднике, передваемых при создании записи в справочнике
    /// </summary>
    public class DTOEmployeeCreateRequest
    {
        /// <summary>
        /// Имя
        /// </summary>        
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>        
        public string LastName { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>        
        public string Email { get; set; }

        /// <summary>
        /// Список ролей
        /// </summary>        
        public List<Guid> Roles { get; set; }

        /// <summary>
        /// Количество примененных промокодов
        /// </summary>             
        public int AppliedPromocodesCount { get; set; }
    }
}