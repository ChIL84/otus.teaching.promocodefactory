﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DTOModels
{
    /// <summary>
    /// DTO класс сведения о сотруднике, направляемые в ответе
    /// </summary>
    public class DTOEmployeeResponse
    {
        /// <summary>
        /// GUID записи о сотруднике в справочнике
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Фамилия и имя сотрудника
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Список ролей сотрудника
        /// </summary>
        public List<DTORoleResponse> Roles { get; set; }

        /// <summary>
        /// Количество примененных промокодов
        /// </summary>
        public int AppliedPromocodesCount { get; set; }
    }
}