﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DTOModels
{
    /// <summary>
    /// DTO класс сведений о сотрднике, передваемых при обновлении записи в справочнике
    /// </summary>
    public class DTOEmployeeUpdateRequest :
        DTOEmployeeCreateRequest
    {
        /// <summary>
        /// Идентификатор записи о сотрднике в справочнике
        /// </summary>        
        public Guid Guid { get; set; }
    }
}
