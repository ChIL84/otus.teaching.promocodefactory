﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.DTOModels
{
    /// <summary>
    /// DTO класс сведений о роле сотрдника, передваемых в ответе
    /// </summary>
    public class DTORoleResponse
    {
        /// <summary>
        /// Идентификатор записи в справочнике ролей
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Наименование роли
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание роли
        /// </summary>
        public string Description { get; set; }
    }
}