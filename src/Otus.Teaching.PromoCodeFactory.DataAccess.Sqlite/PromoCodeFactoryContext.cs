﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Sqlite.Models;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Sqlite
{
    /// <summary>
    /// Контекст данных EF проекта PromoCodeFactory, для доступа к БД
    /// </summary>
    public class PromoCodeFactoryContext: DbContext
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="options"></param>
        public PromoCodeFactoryContext(DbContextOptions<PromoCodeFactoryContext> options): base(options)
        {

        }

        /// <summary>
        /// Набор данных о сотрудниках, хранящихся в БД 
        /// </summary>
        public DbSet<Employee> Employees { get; set; }

        /// <summary>
        /// Набор данных о ролях сотрудников, хранящихся в БД 
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
.               HasKey(e => e.IntId);

            modelBuilder.Entity<Role>()
                .HasKey(e => e.IntId);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Roles)
                .WithMany(r => r.Employees)
                .UsingEntity(cf => cf.ToTable("Employee2Role"));
                //.UsingEntity<Employee2Role>(cf =>
                //{
                //    //cf
                //    //    .HasOne(r => r.Roles)
                //    //    .WithMany()
                //    //    .HasForeignKey(r => r.RolesIntId)
                //    //    .HasConstraintName("FK_Employee2Role_Roles_RolesIntId")
                //    //    .HasPrincipalKey(r => r.IntId)
                //    //    .OnDelete(DeleteBehavior.NoAction);
                //    //cf
                //    //    .HasOne(e => e.Employees)
                //    //    .WithMany()
                //    //    .HasForeignKey(e => e.EmployeesIntId)
                //    //    .HasConstraintName("FK_Employee2Role_Employees_EmployeesIntId")
                //    //    .HasPrincipalKey(e => e.IntId)
                //    //    .OnDelete(DeleteBehavior.Cascade);
                //    cf.ToTable("Employee2Role");
                //    cf.HasKey(k => new { k.RolesIntId, k.EmployeesIntId });
                //});            
        }

        public void SetEntryRangeValues<T>(ICollection<T> existRange, ICollection<T> range) where T : BaseEntity
        {
            foreach (var item in range)
            {
                var eItem = existRange
                    .FirstOrDefault(eItem => eItem.IntId == item.IntId);

                if (eItem == null)
                {
                    existRange.Add(item);
                }
                else
                {
                    this.Entry(eItem).CurrentValues.SetValues(item);
                }
            }            

            IEnumerable<T> exceptedItem = existRange.Except(range);

            if(exceptedItem != null && exceptedItem.Any())
            {
                foreach (T item in exceptedItem)
                {
                    existRange.Remove(item);
                }
            }

        }
    }
}
