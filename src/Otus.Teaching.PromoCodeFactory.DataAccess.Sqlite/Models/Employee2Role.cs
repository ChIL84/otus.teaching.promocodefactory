﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Sqlite.Models
{
    public class Employee2Role
    {
        public int EmployeesIntId { get; set; }

        public Employee Employees { get; set; }

        public int? RolesIntId { get; set; }

        public Role? Roles { get; set; }
    }
}
