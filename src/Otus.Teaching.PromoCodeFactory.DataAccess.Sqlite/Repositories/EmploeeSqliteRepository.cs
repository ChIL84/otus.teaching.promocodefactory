﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Sqlite.Repositories
{
    /// <summary>
    /// Класс репозитория Employee для Sqlite
    /// </summary>
    public class EmploeeSqliteRepository :
        IRepository<Employee>
    {
        #region состояние

        /// <summary>
        /// Контекст данных для взаимодействия в БД
        /// </summary>
        protected readonly PromoCodeFactoryContext _dbContext;

        #endregion

        #region конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="dbContext">контекст данных EF проекта PromoCodeFactory, для доступа к БД</param>
        /// <param name="dbSet"></param>
        /// <exception cref="RepositoryException"></exception>
        public EmploeeSqliteRepository([NotNull] PromoCodeFactoryContext dbContext)
        {
            if (dbContext == null)
            {
                string message = $"При создании объекта класса {nameof(EmploeeSqliteRepository)} " +
                    $"передан нулевой параметр {nameof(dbContext)}";

                throw new RepositoryException(message);
            }

            this._dbContext = dbContext;
        }

        #endregion

        #region методы (реализация IRepository)

        public Task<Employee> CreateAsync(Employee item)
        {
            return Task.Run(() =>
            {
                item.Guid = Guid.NewGuid();

                _dbContext.Employees.Add(item);

                _dbContext.SaveChanges();

                return item;
            });
        }

        public Task<bool> DeleteByGuidAsync(Guid guid)
        {
            return Task.Run(() =>
            {
                int countDeletedRows = _dbContext.Employees
                .Where(item => item.Guid.ToString().ToUpper() == guid.ToString().ToUpper())                
                .ExecuteDelete();

                return countDeletedRows > 0;
            });
        }

        public Task<IEnumerable<Employee>> GetAllAsync()
        {
            return Task.FromResult(_dbContext.Employees
                .Include(x => x.Roles)
                .AsEnumerable());
        }

        public Task<Employee> GetByGuidAsync(Guid guid)
        {
            return Task.FromResult(_dbContext.Employees
                .Include(x => x.Roles)
                .Where(item => item.Guid.ToString().ToUpper() == guid.ToString().ToUpper())
                .FirstOrDefault());
        }

        public Task<Employee> UpdateAsync(Employee item)
        {
            return Task.Run(() =>
            {
                Employee existingItem = _dbContext.Employees
                    .Include(x => x.Roles)
                    .Where(x => x.Guid.ToString().ToUpper() == item.Guid.ToString().ToUpper())
                    .FirstOrDefault();

                if (existingItem == null)
                {
                    throw new RepositoryException($"В справончике сотрудников не найдена запись " +
                        $"с идентификатором '{item.Guid}'");
                }

                item.IntId = existingItem.IntId;

                _dbContext.Entry(existingItem).CurrentValues.SetValues(item);

                _dbContext.SetEntryRangeValues(existingItem.Roles, item.Roles);

                _dbContext.SaveChanges();

                return existingItem;
            });
        } 

        #endregion
    }
}
