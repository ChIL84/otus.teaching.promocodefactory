﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Sqlite.Repositories
{
    /// <summary>
    /// Класс репозитория Role для Sqlite
    /// </summary>
    public class RoleSqliteRepository :
        IRepository<Role>
    {
        #region состояние

        /// <summary>
        /// Контекст данных для взаимодействия в БД
        /// </summary>
        protected readonly PromoCodeFactoryContext _dbContext;

        #endregion

        #region конструктор

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="dbContext">контекст данных EF проекта PromoCodeFactory, для доступа к БД</param>
        /// <param name="dbSet"></param>
        /// <exception cref="RepositoryException"></exception>
        public RoleSqliteRepository([NotNull] PromoCodeFactoryContext dbContext)
        {
            if (dbContext == null)
            {
                string message = $"При создании объекта класса {nameof(RoleSqliteRepository)} " +
                    $"передан нулевой параметр {nameof(dbContext)}";

                throw new RepositoryException(message);
            }

            this._dbContext = dbContext;
        }

        #endregion

        #region методы (реализация IRepository)

        public Task<Role> CreateAsync(Role item)
        {
            return Task.Run(() =>
            {
                item.Guid = Guid.NewGuid();

                _dbContext.Roles.Add(item);

                _dbContext.SaveChanges();

                return item;
            });
        }

        public Task<bool> DeleteByGuidAsync(Guid guid)
        {
            return Task.Run(() =>
            {
                int countDeletedRows = _dbContext.Roles
                .Where(item => item.Guid.ToString().ToUpper() == guid.ToString().ToUpper())                
                .ExecuteDelete();

                return countDeletedRows > 0;
            });
        }

        public Task<IEnumerable<Role>> GetAllAsync()
        {
            return Task.FromResult(_dbContext.Roles
                .AsEnumerable());
        }

        public Task<Role> GetByGuidAsync(Guid guid)
        {
            return Task.FromResult(_dbContext.Roles                
                .Where(item => item.Guid.ToString().ToUpper() == guid.ToString().ToUpper())
                .FirstOrDefault());
        }

        public Task<Role> UpdateAsync(Role item)
        {
            return Task.Run(() =>
            {
                Role sourceItem = _dbContext.Roles                    
                    .Where(x => x.Guid.ToString().ToUpper() == item.Guid.ToString().ToUpper())
                    .FirstOrDefault();

                if (sourceItem == null)
                {
                    throw new RepositoryException($"В справончике сотрудников не найдена запись " +
                        $"с идентификатором '{item.Guid}'");
                }

                sourceItem.FillBy(item);

                _dbContext.SaveChanges();

                return sourceItem;
            });
        } 

        #endregion
    }
}
