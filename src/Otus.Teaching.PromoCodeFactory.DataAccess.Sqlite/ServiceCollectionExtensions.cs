﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Sqlite
{
    /// <summary>
    /// Класс расширения настройки колекции сервисов приложения ASPNetCore
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSqliteDataBase(this IServiceCollection services, string dataSource)
        {
            return services.AddDbContext<PromoCodeFactoryContext>(options =>
            {
                options.UseSqlite(dataSource);
            });
        }
    }
}
