﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Класс исключения репозитория данных
    /// </summary>
    public class RepositoryException: Exception
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="message">сообщение об исключении</param>
        /// <param name="innerException">внутренее исключение</param>
        public RepositoryException(string message = null, Exception innerException = null)
            : base(message, innerException) { }
    }
}
