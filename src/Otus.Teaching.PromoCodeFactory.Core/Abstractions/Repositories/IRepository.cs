﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    /// <summary>
    /// Универсальный интерфейс репозитория
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T>
        where T: BaseEntity
    {
        /// <summary>
        /// Получить все объекты репозитория
        /// </summary>
        /// <returns></returns>
        /// <exception cref="RepositoryException"></exception>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Получить объект репозитория по Id
        /// </summary>
        /// <param name="guid">GUID объекта, для которого получаем информацию</param>
        /// <returns></returns>
        /// <exception cref="RepositoryException"></exception>
        Task<T> GetByGuidAsync(Guid guid);

        /// <summary>
        /// Удалить объект репозитория по Id
        /// </summary>
        /// <param name="guid">GUID удаляемого объекта</param>
        /// <returns></returns>
        /// <exception cref="RepositoryException"></exception>
        Task<bool> DeleteByGuidAsync(Guid guid);

        /// <summary>
        /// Добавить объект в репозиторий
        /// </summary>
        /// <param name="item">объект, добавляемый в репозиторий</param>
        /// <returns></returns>
        /// <exception cref="RepositoryException"></exception>
        Task<T> CreateAsync(T item);

        /// <summary>
        /// Обновить объект в репозитории
        /// </summary>
        /// <param name="item">объект, обновляемый в репозитории</param>
        /// <returns></returns>
        /// <exception cref="RepositoryException"></exception>
        Task<T> UpdateAsync(T item);
    }
}