﻿using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    /// <summary>
    /// Класс сведений о сотруднике
    /// </summary>
    public class Employee
        : BaseEntity
    {
        #region свойства класса

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Полное имя
        /// </summary>
        /// <remarks>получается путем объединения FirstName и LastName</remarks>
        public string FullName => $"{FirstName} {LastName}";

        /// <summary>
        /// Адрес электронной почты
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Список ролей сотрудника
        /// </summary>
        public ICollection<Role> Roles { get; set; }

        /// <summary>
        /// Количество прмененных промодов
        /// </summary>
        public int AppliedPromocodesCount { get; set; }

        #endregion

        #region методы класса

        /// <summary>
        /// Заполнить текущий объект на основании другого
        /// </summary>
        /// <param name="item">объект, служащий основой данных для текущего объекта</param>
        public override void FillBy(BaseEntity item)
        {
            if (!(item is Employee dst)) { return; }

            FirstName = dst.FirstName;

            LastName = dst.LastName;

            Email = dst.Email;

            Roles = null;

            if (dst.Roles != null && dst.Roles.Count() > 0)
            {
                List<Role> rolesLst = new List<Role>();

                foreach (Role role in dst.Roles.Where(x => x != null))
                {
                    rolesLst.Add(new Role(role));
                }

                Roles = rolesLst;
            }

            AppliedPromocodesCount = dst.AppliedPromocodesCount;
        }

        #endregion
    }
}