﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    /// <summary>
    /// Класс сведений о роли сотрудника
    /// </summary>
    public class Role
        : BaseEntity
    {
        #region конструкторы

        /// <summary>
        /// Конструктор по умолчанию
        /// </summary>
        public Role() { }
        
        /// <summary>
        /// Конструктор иницирующий объект класса
        /// </summary>
        /// <param name="role">иницирующий объект</param>
        public Role(Role role):base() 
        {
            if (role == null) return;

            FillBy(role);

            Guid = role.Guid;
        }

        #endregion

        #region свойства класса

        /// <summary>
        /// Наименование роли
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание роли
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Список сотрудников, в роли
        /// </summary>
        public ICollection<Employee>? Employees { get; set; }

        #endregion

        #region методы класса

        /// <summary>
        /// Заполнить текущий объект на основании другого
        /// </summary>
        /// <param name="item">объект, служащий основой данных для текущего объекта</param>
        public override void FillBy(BaseEntity item)
        {
            if (!(item is Role src)) { return; }

            Name = src.Name;

            Description = src.Description;
        }

        #endregion
    }
}