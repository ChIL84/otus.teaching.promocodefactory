﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    /// <summary>
    /// Абстрактный базовый класс доменных сущностей
    /// </summary>
    public abstract class BaseEntity
    {
        #region свойство класса

        /// <summary>
        /// Guid объекта
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Идентифификатор объекта в БД
        /// </summary>
        /// <remarks>данный идентификатор был добавлен из-за проблем использования GUID 
        /// в качестве primary key в Sqlite</remarks>
        public int IntId { get; set; }

        #endregion

        #region методы класса

        /// <summary>
        /// Заполнить текущий объект на основании другого
        /// </summary>
        /// <param name="item">объект, служащий основой данных для текущего объекта</param>
        public abstract void FillBy(BaseEntity item);

        #endregion
    }
}